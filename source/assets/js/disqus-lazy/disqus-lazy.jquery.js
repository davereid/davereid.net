(function ($, window) {

    "use strict";

    var disqusThread = $('#disqus_thread'),
        disqusLazyLoadOffset = disqusLazyLoadOffset || 100;

    function loadDisqus(callback) {
        if (!disqusThread.hasClass('disqus-lazy-processed')) {
            disqusThread.addClass('disqus-lazy-processed');
            $.ajax({
                type: 'GET',
                url: '//' + disqus_shortname + '.disqus.com/embed.js',
                dataType: 'script',
                cache: false,
                success: callback
            });
        }
    }

    if (window.location.hash === '#disqus_thread') {
        loadDisqus(function () {
            $('html, body').animate({
                scrollTop: disqusThread.offset().top
            }, 2000);
        });
    }
    else {
        $(window).scroll(function scrollHandler(event) {
            if ($(window).scrollTop() + $(window).height() > disqusThread.offset().top - disqusLazyLoadOffset) {
                loadDisqus();
                $(window).unbind('scroll', scrollHandler);
            }
        });
    }

})(jQuery, this);
