# davereid.net

My personal website, built with [Sculpin](http://sculpin.io).

# Deployments

Managed with CodeShip

[ ![Codeship Build Status](https://codeship.com/projects/02a19a80-171d-0133-391f-1ec1a2fe89bc/status?branch=master)](https://codeship.com/projects/93472)

[ ![drone.io Build Status](https://drone.io/bitbucket.org/davereid/davereid.net/status.png)](https://drone.io/bitbucket.org/davereid/davereid.net/latest)
